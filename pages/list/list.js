// pages/shoplist/shoplist.js
Page({

  /**
    * 页面的初始数据
    */
  data:{

    query:{},
    "list":[
      {
        "id":"1",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"2",
        "name":"从南京南到夫子庙",
        "image":"/image/showList/test2.png",
        "address":"南京·夫子庙",
        "tagList":["南京","秦淮风光","拥挤"],
        "slogan":"你一句春不晚，我被堵噶在南京南。。。",
        "price":"￥19.9"
      },
      {
        "id":"3",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"4",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"5",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"6",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"7",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"8",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"9",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"10",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"11",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"12",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"
      },
      {
        "id":"13",
        "name":"线路1",
        "image":"/image/showList/test1.jpg",
        "address":"北京·西城区",
        "tagList":["北京","好玩"],
        "slogan":"欢迎来到北京玩耍，西城区旅游度假胜地",
        "price":"￥39.9"}],
    i:0,//表示当前item项个数
    shoplist:[],
    result:[],
    isloading:false
  },
  /**
    * 生命周期函数--监听页面加载
    */
  onLoad(options) {
    this.setData
    ({
      query:options
    })
    this.getshoplist()

  },
  getshoplist()
  {
    //当页面加载完毕时停止继续发出请求
    if(this.data.i>this.data.result.length)
    {
      return wx.showToast({
        title: '数据加载完毕！',
        icon:'none'
      })
    }
    //防止多次下滑触底从而发出多次请求
    this.setData({isloading:true}),
        wx.showLoading({
          title: '数据加载中...',
        })
    this.data.shoplist=this.data.list.slice(this.data.i,this.data.i+10)
    this.setData({
      i:this.data.i + 10
    })
    this.data.result=this.data.result.concat(this.data.shoplist)
    this.setData({
      result:this.data.result
    })
    wx.hideLoading()
    this.setData({isloading:false})
  },

  /**
    * 生命周期函数--监听页面初次渲染完成
    */
  onReady() {
    wx.setNavigationBarTitle({
      title: this.data.query.title,
    })
  },

  /**
    * 生命周期函数--监听页面显示
    */
  onShow() {

  },

  /**
    * 生命周期函数--监听页面隐藏
    */
  onHide() {

  },

  /**
    * 生命周期函数--监听页面卸载
    */
  onUnload() {

  },

  /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
  onPullDownRefresh() {

  },

  /**
    * 页面上拉触底事件的处理函数
    */
  onReachBottom() {
    if(this.data.isloading) return
    this.getshoplist()
  },

  /**
    * 用户点击右上角分享
    */
  onShareAppMessage() {

  }
})